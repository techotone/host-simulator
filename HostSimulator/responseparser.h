#pragma once
#include <string>
#include <list>
#include <map>
#include <memory>



class parser
{
private:
	static std::shared_ptr<parser> instanceptr;
	static const int BUFFER_SIZE = 1024;

public:
	std::shared_ptr <std::map<int, std::map<int, std::string>>> parse(const std::string &file);
	static std::shared_ptr<parser> get_intance();
};


