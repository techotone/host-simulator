﻿#pragma once
#include <vector>
#include <memory>
#include <iostream>
#include <map>

#include "defs.h"

class iso8583
{
private:

	static const int TYPE_BCD = 1;
	static const int TYPE_ASC = 2;
	static const int TYPE_BIN = 4;
	// 8
	// 16
	static const int TYPE_LEN = 32;
	static const int TYPE_LEN_LEN = 64;
	// 128
	static const int TYPE_FILL_SPACE_RIGHT = 128;
	// 256
	static const int TYPE_FILL_SPACE_LEFT = 256;
	// 512
	static const int TYPE_FILL_ZERO_RIGHT = 512;
	// 1024
	static const int TYPE_FILL_ZERO_LEFT = 1024;
	static const int TYPE_L_BCD = TYPE_LEN + TYPE_BCD;
	static const int TYPE_LL_BCD = TYPE_LEN_LEN + TYPE_BCD;
	static const int TYPE_L_ASC = TYPE_ASC + TYPE_LEN;
	static const int TYPE_LL_ASC = TYPE_ASC + TYPE_LEN_LEN;
	static const int TYPE_L_BIN = TYPE_BIN + TYPE_LEN;
	static const int TYPE_LL_BIN = TYPE_BIN + TYPE_LEN_LEN;
	static const int TYPE_ASC_FS = TYPE_ASC + TYPE_FILL_SPACE_RIGHT;

	static const int ATTR_INDEX_TYPE = 0;
	static const int ATTR_INDEX_LEN_DEFAULT = 1;
	static const int BITMAP_FIELD_INDEX_ = 1;

	static const int ISO_BIT_MAX = 64;


	 int FIELD_ATTRIBUTE_ARRAY[65][4] =
	{// type, length, not defined, not defined
		{ TYPE_BCD	,	4, 0, 0 },						// field	0, (Message Type Identifier)
		{ TYPE_BIN	,	8, 0, 0 },						// field	1, bitmap
		{ TYPE_L_BCD	,	19, 0, 0 },					// field	2, (Primary Account Number), N..19(LLVAR)，2个字节的长度值＋最大19个字节的主账号，压缩时用BCD码表示的1个字节的长度值＋用左靠BCD码表示的最大10个字节的主账号。
		{ TYPE_BCD	,	6, 0, 0 },						// field	3, (Transaction Processing Code), N6，6个字节的定长数字字符域，压缩时用BCD码表示的3个字节的定长域。
		{ TYPE_BCD	,	12, 0, 0 },						// field	4, (Amount Of Transactions), N12，12个字节的定长数字字符域，压缩时用BCD码表示的6个字节的定长域。
		{ 0	,	0, 0, 0 },								// field	5
		{ 0	,	0, 0, 0 },								// field	6
		{ 0	,	0, 0, 0 },								// field	7
		{ 0	,	0, 0, 0 },								// field	8
		{ 0	,	0, 0, 0 },								// field	9
		{ 0	,	0, 0, 0 },								// field	10
		{ TYPE_BCD	,	6, 0, 0 },						// field	11, 受卡方系统跟踪号(System Trace Audit Number), N6，6个字节的定长数字字符域，压缩时用BCD码表示的3个字节的定长域。
		{ TYPE_BCD	,	6, 0, 0 },						// field	12, 受卡方所在地时间(Local Time Of Transaction), hhmmss, N6，6个字节的定长数字字符域，压缩时用BCD码表示的3个字节的定长域。
		{ TYPE_BCD	,	4, 0, 0 },						// field	13, 受卡方所在地日期(Local Date Of Transaction), MMDD, N4，4个字节的定长数字字符域，压缩时用BCD码表示的2个字节的定长域。
		{ TYPE_BCD	,	4, 0, 0 },						// field	14, 卡有效期(Date Of Expired), YYMM, N4，4个字节的定长数字字符域，压缩时用BCD码表示的2个字节的定长域。格式：YYMM。
		{ TYPE_BCD	,	4, 0, 0 },						// field	15, 清算日期(Date Of Settlement), N4，4个字节的定长数字字符域，压缩时用BCD码表示的2个字节的定长域。格式：MMDD。,
		{ 0	,	0, 0, 0 },								// field	16
		{ 0	,	0, 0, 0 },								// field	17
		{ 0	,	0, 0, 0 },								// field	18
		{ 0	,	0, 0, 0 },								// field	19
		{ 0	,	0, 0, 0 },								// field	20
		{ 0	,	0, 0, 0 },								// field	21
		{ TYPE_BCD	,	4, 0, 0 },						// field	22, 服务点输入方式码(Point Of Service Entry Mode), N3，3个字节的定长数字字符域，压缩时用左靠BCD码表示的2个字节的定长域。
		{ TYPE_BCD	,	4, 0, 0 },						// field	23, 卡序列号(Card Sequence Number), N3，3个字节的定长数字字符域，压缩时用右靠BCD码表示的2个字节的定长域。
		{ TYPE_BCD	,	4, 0, 0 },						// field	24
		{ TYPE_BCD	,	2, 0, 0 },						// field	25, 服务点条件码(Point Of Service Condition Mode), N2，2个字节的定长数字字符域，压缩时用左靠BCD码表示的1个字节的定长域。
		{ TYPE_BCD	,	2, 0, 0 },						// field	26, 服务点PIN获取码(Point Of Service PIN Capture Code), N2，2个字节的定长数字字符域，压缩时用BCD码表示的1个字节的定长域。
		{ TYPE_BCD	,	2, 0, 0 },						// field	27
		{ TYPE_BCD	,	2, 0, 0 },						// field	28
		{ TYPE_ASC	,	8, 0, 0 },						// field	29
		{ TYPE_BCD	,	8, 0, 0 },						// field	30
		{ TYPE_BCD	,	8, 0, 0 },						// field	31
		{ TYPE_L_BCD	,	11, 0, 0 },					// field	32, 受理机构标识码(Acquiring Institution Identification Code), N..11(LLVAR)，2个字节的长度值＋最大11个字节的受理方标识码，压缩时用BCD码表示的1个字节的长度值＋用左靠BCD码表示的最大6个字节的受理方标识码。
		{ TYPE_L_BCD	,	11, 0, 0 },					// field	33
		{ TYPE_L_BCD	,	28, 0, 0 },					// field	34
		{ TYPE_L_BCD	,	37, 0, 0 },					// field	35, 2磁道数据(Track 2 Data), Z..37(LLVAR)，2个字节的长度值＋最大37个字节的第二磁道数据(数字和分隔符)，压缩时用BCD码表示的1个字节的长度值＋用左靠BCD码表示的最大19个字节的第二磁道数据
		{ TYPE_LL_BCD	,	104, 0, 0 },				// field	36, 3磁道数据(Track 3 Data), Z...104(LLLVAR)，3个字节的长度值＋最大104个字节的第三磁道数据(数字和分隔符)，压缩时用右靠BCD码表示的2个字节的长度值＋用左靠BCD码表示的最大52个字节的第三磁道数据。
		{ TYPE_ASC_FS	,	12, 0, 0 },					// field	37, 检索参考号(Retrieval Reference Number), AN12，12个字节的定长字符域
		{ TYPE_ASC_FS	,	6, 0, 0 },					// field	38, 授权标识应答码(Authorization Identification Response Code), AN6，6个字节定长的字母、数字和特殊字符。
		{ TYPE_ASC_FS	,	2, 0, 0 },					// field	39, 应答码(Response Code), AN2，2个字节的定长字符域。
		{ TYPE_ASC	,	3, 0, 0 },						// field	40
		{ TYPE_ASC_FS	,	8, 0, 0 },					// field	41, 受卡机终端标识码(Card Acceptor Terminal Identification), ANS8，8个字节的定长的字母、数字和特殊字符。
		{ TYPE_ASC_FS	,	15, 0, 0 },					// field	42, 受卡方标识码(Card Acceptor Identification Code), ANS15，15个字节的定长的字母、数字和特殊字符。
		{ TYPE_ASC	,	3, 0, 0 },						// field	43
		{ TYPE_ASC	,	25, 0, 0 },						// field	44, 附加响应数据(Additional Response Data), AN..25，2个字节长度+ 最大25个字节的数据。压缩时用右靠BCD码表示的1个字节的长度值＋用ASCII码表示的最大25个字节的数据。
		{ TYPE_L_ASC	,	76, 0, 0 },					// field	45
		{ TYPE_LL_ASC	,	999, 0, 0 },				// field	46
		{ TYPE_LL_ASC	,	999, 0, 0 },				// field	47, 营销信息域, 该域是一个变长域（LLLVAR），最长可达999个字节，最开始是一个占3个字节的长度值信息。压缩时采用右靠BCD码表示长度信息，长度信息占两个字节。
		{ TYPE_LL_ASC	,	999, 0, 0 },				// field	48, 附加数据 - 私有(Additional Data - Private), N...322(LLLVAR)，3个字节长度+ 最大322个字节的数据。压缩时用右靠BCD码表示的2个字节的长度值＋用左靠BCD码表示的最大161个字节的数据。
		{ TYPE_ASC	,	3, 0, 0 },						// field	49, 交易货币代码(Currency Code Of Transaction), AN3，3个字节的定长字符域。
		{ TYPE_ASC	,	3, 0, 0 },						// field	50
		{ TYPE_ASC	,	3, 0, 0 },						// field	51
		{ TYPE_BIN	,	8, 0, 0 },						// field	52, 个人标识码数据(PIN Data), B64，8个字节的定长二进制数域。
		{ TYPE_BCD	,	16, 0, 0 },						// field	53, 安全控制信息(Security Related Control Information ), n16，16个字节的定长数字字符域。压缩时用BCD码表示的8个字节的定长域。
		{ TYPE_LL_ASC	,	20, 0, 0 },					// field	54, 余额(Balanc Amount), AN...020(LLLVAR)，3个字节的长度值＋最大20个字节的数据。压缩时用右靠BCD码表示的2个字节的长度值＋用ASCII码表示的最大20个字节的数据。
		{ TYPE_LL_BIN	,	255, 0, 0 },				// field	55, IC卡数据域(Intergrated Circuit Card System Related Data), 该域是一个变长域（LLLVAR），最长可达255个字节，最开始是一个占3个字节的长度值信息。压缩时采用右靠BCD码表示长度信息，长度信息占两个字节。
		{ TYPE_LL_ASC	,	999, 0, 0 },				// field	56
		{ TYPE_LL_ASC	,	999, 0, 0 },				// field	57
		{ TYPE_LL_ASC	,	100, 0, 0 },				// field	58, PBOC电子钱包标准的交易信息（PBOC_ELECTRONIC_DATA）, ans...100(LLLVAR)，3个字节的长度值＋最大100个字节的字母、数字字符、特殊符号，压缩时采用右靠2个字节表示长度值。
		{ TYPE_LL_ASC	,	999, 0, 0 },				// field	59, 自定义域(Reserved Private), 该域是一个变长域（LLLVAR），最长可达999字节，最开始是一个占3个字节的长度值信息。压缩时采用右靠BCD码表示长度信息，长度信息占两个字节。
		{ TYPE_LL_ASC	,	17, 0, 0 },					// field	60, 自定义域(Reserved Private), N...17(LLLVAR)，3个字节的长度值＋最大17个字节的数字字符域。压缩时用右靠BCD码表示的2个字节的长度值＋用左靠BCD码表示的最大9个字节的数据。
		{ TYPE_LL_BCD	,	29, 0, 0 },					// field	61, 原始信息域(Original Message), N...029(LLLVAR)，3个字节的长度值＋最大29个字节的数字字符域，压缩时用右靠BCD码表示的2个字节的长度值＋用左靠BCD码表示的最大15个字节的数据。
		{ TYPE_LL_ASC	,	512, 0, 0 },				// field	62, 自定义域(Reserved Private), ANS...512(LLLVAR)，3个字节的长度值＋最大512个字节的数据域。压缩时用右靠BCD码表示的2个字节的长度值＋用ASCII码表示的最大512个字节的数据。
		{ TYPE_LL_ASC	,	163, 0, 0 },				// field	63, 自定义域(Reserved Private), ANS...163(LLLVAR)，3个字节的长度值＋最大163个字节的数据。压缩时用右靠BCD码表示的2个字节的长度值＋用ASCII码表示的最大163个字节的数据。
		{ TYPE_BIN	,	8, 0, 0 },						// field	64, 报文鉴别码(Message Authentication Code), B64，8个字节的定长域
	};


	public :
		static std::shared_ptr<iso8583> getInstance() 
		{
			if (instance == nullptr)
				instance = std::make_shared<iso8583>();

			return instance;
		}

		//get the instance of the object
		std::map<int, std::string> unpack(const std::vector<byte> &connection_bytes);
		std::vector<byte> pack(std::map<int, std::string>& iso_map, std::map<int, std::string>& overried_fields);
		


	private:
		static std::shared_ptr<iso8583> instance;
		static void print_raw_packet(const std::vector<byte> & bytes);

};

