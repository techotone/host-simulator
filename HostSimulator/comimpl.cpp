#include "comimpl.h"
#include <iostream>
#include "iso8583.h"


void commimpl::start_listening(int port)
{
	if (is_started())
		return;

	reset_stop();
	listen_thread = std::make_unique<std::thread>(&comm::listen,this,port);
}

void commimpl::stop_listening()
{
	stop(); //signla the server to stop

	update_status("waiting for the listener to stop");


	if (listen_thread->joinable())
		listen_thread->join();

}

void commimpl::set_custom_field_map(std::shared_ptr<std::map<int, std::map<int, std::string>>> mp)
{
	cust_field_map = mp;
}

void commimpl::update_status(std::string s)
{
	std::cout << s << std::endl;
}

void commimpl::client_data_buffer(std::vector<byte>& buffer)
{
	try
	{
		::Beep(2000, 100);
	
		std::shared_ptr<iso8583> isoptr = iso8583::getInstance();
		std::map<int, std::string> iso_map = isoptr->unpack(buffer);

		//here we set the custom fields using the custom field map;
		//get the message type of the source message
		std::map<int, std::string> override_map;

		auto it = iso_map.find(1);
		if (it != iso_map.end())
		{
			//check whether we have a overriden message for this message type
			int msgtype = std::stoi(it->second);
			auto a = cust_field_map->find(msgtype);
			if (a != cust_field_map->end())
				//we have a custom overriden message so we set accordingly
				//get a  copy of message 
				override_map = a->second;
					
		}
	
		std::vector<byte> t = isoptr->pack(iso_map,override_map);
		send(t);
	}
	catch (std::exception &ex)
	{
		std::cout << "exception occured " << ex.what() << std::endl;
	}
	

	//pack and send 

}
