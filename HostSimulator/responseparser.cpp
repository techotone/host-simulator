#include "responseparser.h"
#include <fstream>
#include "defs.h"
#include <algorithm>

std::shared_ptr<parser> parser::instanceptr = std::shared_ptr<parser>(nullptr);

std::shared_ptr<std::map<int, std::map<int, std::string>>> parser::parse(const std::string & file)
{
	//create the base map
	std::shared_ptr<std::map<int, std::map<int, std::string>>> basemap = std::make_shared<std::map<int, std::map<int, std::string>>>();

	std::ifstream infile(file);
	if (!infile.is_open())
		throw std::exception("file not found");

	std::string line;

	while (getline(infile, line))
	{
		//find the message id
		int index = line.find("msgid");
		if (index == std::string::npos)
			continue;

		std::string msgid = line.substr(8, 4);
		int imsgid = std::stoi(msgid);

		std::map<int, std::string> msg;

		while (true)
		{
			//go until we find the . stop location
			getline(infile, line);


			if (line.length() == 0)
				continue;

			if ((index = line.find(".")) >= 0)	//end of the current message
			{
				if (msg.size() > 0)
					basemap->insert(std::pair<int, std::map<int, std::string>>(imsgid, msg));
				break;
			}

			
			//expecting field number and fields
			std::string sfield = line.substr(0, 3);		//field number
			std::string sdata = line.substr(5, line.length());

			sdata.erase(std::remove(sdata.begin(), sdata.end(), '"'), sdata.end());

			int field = std::stoi(sfield);
			msg.insert(std::pair<int, std::string>(field, sdata));

		}
	}

	return basemap;
}

std::shared_ptr<parser> parser::get_intance()
{
	if (instanceptr == nullptr)
		instanceptr = std::make_shared<parser>();

	return instanceptr;

}


