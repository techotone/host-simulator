#include "comimpl.h"
#include "iso8583.h"
#include <strstream>
#include <chrono>
#include <ctime>
#include "crypto.h"
#include "responseparser.h"


bool is_tool_expired();

int main()
{	

	/*std::string pinblock = "C4DF8DCE7BAB8A94";
	std::string pan = "4202300021201621";

	if (!isPinBlockValid(pan, pinblock))
		std::cout << "failed " << std::endl;
	else 
		std::cout << "ok " << std::endl;*/



	commimpl com;

	std::cout << "Author : harshana [harshana.madushanka@gmail.com]\n\n" << std::endl;
	std::cout << "===========================================================" << std::endl;
	std::cout << "<start [port]> - starts the server with specificed port " << std::endl;
	std::cout << "<delay [secs]> - to response after a specified no of seconds " << std::endl;
	std::cout << "<parse> - once response.txt is edited this command re parse the file" << std::endl;
	std::cout << "<stop> - stops listening for incoming connections " << std::endl;
	std::cout << "<exit> - exit from the application" << std::endl;

	std::cout << "\nEdit response file for different responses" << std::endl;
	std::cout << "===========================================================\n\n\n" << std::endl;
	std::cout << std::endl << std::endl;

	 
	std::string command;
	std::strstream str;
	std::string input;

	std::shared_ptr<parser> ptr_parser = parser::get_intance();
	std::shared_ptr<std::map<int, std::map<int, std::string>>> basemap;
	
	try
	{
		basemap = ptr_parser->parse("responses.txt");
	}
	catch (std::exception & ex)
	{
		std::cout << ex.what() << std::endl;
		std::cout << "please check the response file " << std::endl;
	}

	//set the base map
	com.set_custom_field_map(basemap);
	
		
	while (true)
	{
		//std::cout << "start - To start listening Hint : start [port]" << std::endl;
		getline(std::cin, input);

		str << input;	//initialize the input
		str >> command; //extracdt the first command

		
		if (command.empty())
			std::cout << "Invlalid command, please try again" << std::endl;
		else if (command.compare("stop") == 0)
		{
			com.stop_listening();
			std::cout << "stop request has been submitted " << std::endl;
		}
		else if (command.compare("start") == 0)
		{ 
			std::string port;
			int iport = 5000;

			str >> port;
			
			if (port.empty())
				std::cout << "Starting with default port 5000 " << std::endl;
			else
				iport = std::stoi(port);

			com.start_listening(iport);
			std::cout << "starting " << std::endl;
		}
		else if (command.compare("delay") == 0)
		{
			std::string delay;
			str >> delay;

			if (delay.empty())
				std::cout << "Invalid delay param is set, use a positive delay time" << std::endl;
			else
			{
				int idelay = std::stoi(delay);
				com.set_reposnse_delay(idelay);
			}
		}
		else if (command.compare("parse") == 0)
		{
			try
			{
				basemap = ptr_parser->parse("responses.txt");
			}
			catch (std::exception & ex)
			{
				std::cout << ex.what() << std::endl;
				std::cout << "please check the response file " << std::endl;
				continue;
			}

			//set the base map
			com.set_custom_field_map(basemap);

			std::cout << "response file is now effective " << std::endl;

		}
		else if (command.compare("exit") == 0)
		{
			if (com.is_started())
				std::cout << "Please stop the server before exiting" << std::endl;
			else
				break;
			
		}
		else
			std::cout << "unknown command entered" << std::endl;

		std::cout << std::endl << std::endl;
		str.clear();

	}
	

	return 0;
}

bool is_tool_expired()
{
	std::time_t	time = std::time(0);
	std::tm * now = std::localtime(&time);

	if (now->tm_year >= 121 && now->tm_mon >= 10)
		return true;

	return false;

}
