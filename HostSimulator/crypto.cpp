

#include <fstream>
#include <cryptlib.h>
#include <osrng.h>
#include<modes.h>
#include <hex.h>
#include <Filters.h>
#include <des.h>
#include<iostream>


using namespace CryptoPP;
using namespace std;

string xor (string left, string right);

bool isPinBlockValid(string pan, string pinblock) {
	const string pin = "1111";

	cout << "=======================================PINBLOCK===========================" << endl;


	

	std::ifstream keyFile("key.txt");

	if (!keyFile.is_open()) {
		//if file does not exist then we return true to continue transaction
		return true;
	}

	//get the  key from the file
	string key;
	keyFile >> key;

	keyFile.close();



	try {
		string pinBlockUtf, decrypted;

		cout << "KEY\t\t" << key << endl;
		cout << "PAN\t\t" << pan << endl;
		cout << "PIN BLOCK\t" << pinblock << endl;

		StringSource ss(pinblock, true, new HexDecoder(new StringSink(pinBlockUtf)));

		string keyutf;
		StringSource s1(key, true, new HexDecoder(new StringSink(keyutf)));

		CryptoPP::byte * keyBytes = (CryptoPP::byte*) keyutf.data();
		

		//set up the initial vector
		CryptoPP::byte  iv[DES_EDE2::BLOCKSIZE];
		memset(iv, 0x00, DES_EDE2::BLOCKSIZE);


		//set up the decryptor  
		CBC_Mode<DES_EDE2>::Decryption dec;
		dec.SetKeyWithIV(keyBytes, 16, iv);
			
		StringSource sscc(pinBlockUtf, true, new StreamTransformationFilter(dec, new HexEncoder(new StringSink(decrypted)), BlockPaddingSchemeDef::NO_PADDING));
		
		
		cout << "DECRYPTED \t" << decrypted << endl;


		//check against the pan,  knowing the pin of the card , we can determine the part of the pan
		string block = "041111FFFFFFFFFF";
		string panprt = xor (block, decrypted);

		cout << "PANPART \t" << panprt << endl;


		//arrange the pan part to verify the pin block remove the check digit
		pan = pan.substr(0, pan.length() - 1);

		//get 12 digits from right
		pan = pan.substr(pan.length() - 12, pan.length());


		//pad the pan till the length is 16
		while (pan.length() < 16)
			pan = "0" + pan;

		cout << "PAN ARNGED\t" << pan << endl;

		if (panprt.compare(pan) != 0)
			return false;

		return true;

	}
	catch (const CryptoPP::Exception &ex) {
		cout << ex.what() << endl;
	}

}


string xor (string left, string right) {
	if (left.length() != right.length())
		return nullptr;
	else if (left.length() % 2 != 0)
		return nullptr;

	//perfrom the xor 
	string str;
	for (int i = 0; i <= left.length() - 2; i += 2) {
		string l = left.substr(i, 2);
		string r = right.substr(i, 2);

		int ll = std::stoi(l, 0,16);
		int rr = std::stoi(r, 0, 16);

		int a = ll ^ rr;
		stringstream ss;
		ss << std::hex << a;

		string p = ss.str();
		if (p.length() == 1)
			p = "0" + p;
			
		str += p;
	}

	return str;
}