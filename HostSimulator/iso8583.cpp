﻿#include "iso8583.h"
#include "byteutils.h"
#include <algorithm>
#include <iterator>
#include "crypto.h"

using namespace std;

std::map<int, std::string> iso8583::unpack(const std::vector<byte>& bytes)
{
	cout << "RAW PACKET [incoming]" << endl;
	cout << "======================" << endl;
	
	print_raw_packet(bytes);
	

	//std::string packet = byteutils::byte_to_hex(bytes);
	//get the length of the iso packet
	int length = 0;
	length |= bytes[0];
	length <<= 8;
	length |= bytes[1];
	
	cout << "Length => " << length << " bytes" << endl;

	std::map<int, std::string> field_map;
	
	//get the tpdu 5 bytes
	std::string tpdu = byteutils::byte_to_hex(bytes, 2,5);
	field_map.insert(std::pair<int, std::string>(0, tpdu));

	cout << "TPDU " << tpdu << endl;

	//get the message type 2 bytes 
	std::string message_type = byteutils::byte_to_hex(bytes, 7, 2);
	field_map.insert(std::pair<int, std::string>(1, message_type));

	cout << "MTI " << message_type << endl;


	//extract the bitmap
	byte bitmap[8];

	//extract the bitmap from the vector
	std::copy(bytes.begin() + 9, bytes.begin() + 9 + 8, bitmap);

	std:string bt = byteutils::byte_to_hex(bytes, 9, 8);
	cout << "BITMAP " << bt << endl << endl;



	int byte_selector = 0;
	int bit_selector = 0;

	int offset = 17;

	//start parsing the fields 
	for (int field = 1; field < 65; field++)
	{
		byte_selector = (field - 1) / 8;
		bit_selector = (field - 1) % 8;

		byte temp = 1 << 7;
		byte b = bitmap[byte_selector];

		std::string data = "";

		if (b & (temp >> bit_selector))
		{
			//introduce a small delay to animate
			_sleep(20);
			int len = 0;
			//refer the look up table if the bit is on 
			int type = FIELD_ATTRIBUTE_ARRAY[field][ATTR_INDEX_TYPE];
			len = FIELD_ATTRIBUTE_ARRAY[field][ATTR_INDEX_LEN_DEFAULT];
			if (type == TYPE_BCD)
			{
				//get the length of the field	
				len /= 2;
				data = byteutils::byte_to_hex(bytes, offset, len);
			}
			else if (type == TYPE_L_BCD)
			{
				length = 0;
				length = bytes[offset];		//get the actual length

				std::string len_string = byteutils::byte_to_hex(bytes, offset, 1);
				length = std::stoi(len_string);

				if (length > len)
					throw std::exception("invalid length for field " + field);

				offset++; 
				len = length;
				if (len % 2 != 0) len++;
				len /= 2;

				data = byteutils::byte_to_hex(bytes, offset, len);
			}
			else if (type == TYPE_LL_BIN)
			{
				length = 0;

				//get the hex value of the length
				std::string str_hex_len = byteutils::byte_to_hex(bytes, offset, 2);
				length = std::stoi(str_hex_len);
				if (length > len)		//if length greater than the default max length error
					throw std::exception("invalid length for the field " + field);

				len = length;
				offset += 2;

				data = byteutils::byte_to_hex(bytes, offset, len);
			}
			else if (type == TYPE_BIN) {
				length = 0;
				data = byteutils::byte_to_hex(bytes, offset, len);
		
			}
			else if (type == TYPE_ASC_FS)
			{
				data = byteutils::byte_to_hex(bytes, offset, len);
				data = byteutils::ascii_to_string(data);
			}
			else if (type == TYPE_LL_ASC)
			{
				//get the variable length of the field
				length = 0;
				length |= bytes[offset];
				length <<= 8;
				length |= bytes[offset + 1];

				std::string hexlen = byteutils::int_to_hex_string(length);
				length = std::stoi(hexlen);
				
				offset += 2;
				len = length;
				data = byteutils::byte_to_hex(bytes, offset, len);
				data = byteutils::ascii_to_string(data);
			}

			offset += len;
			cout << "FIELD [" << std::setw(2) << std::setfill('0') << field << "] " << data << endl;

			//set the field in the map
			field_map.insert(std::pair<int, std::string>(field, data));

		}
	}

	return field_map;

}





std::vector<byte> iso8583::pack(std::map<int, std::string>& iso_map,std::map<int,std::string> & override_fields)
{
	auto it = iso_map.find(0);
	if (it == iso_map.end())
		throw std::exception("tpdu is not in the source message");

	std::string tpdu = it->second;

	//=============================================================
	//analyze and set the response code based on the pin block 
	it = iso_map.find(52);
	if (it != iso_map.end()) {
		std::string pinblock = it->second;

		it = iso_map.find(2);
		if (it != iso_map.end()) {
			std::string pan = it->second;

			bool pinBlockValidationResult = isPinBlockValid(pan, pinblock);

			if (!pinBlockValidationResult) {
				cout << "**************************************************\nPIN BLOCK validation failed, declining the trasaction\n";

				iso_map.insert(std::pair<int, string>(39, "54"));
			}
		}

		//remove the 52 field from the reply packet
		iso_map.erase(52);
		override_fields.erase(52);
	}


	//=============================================================

	//get the override fields and set in the original map
	for (std::pair<int, std::string> p : override_fields)
	{
		//check if the original mssage having the same field, if not we set that field to be sent
		int field = p.first;
		auto it = iso_map.find(field);
		if (it == iso_map.end())
			iso_map.insert(std::pair<int, std::string>(p.first, p.second));
	}


	//check whether the original message have additional fields other than overriden field set
	auto iter = iso_map.begin();
	while (iter++ != iso_map.end())
	{
		if (iter == iso_map.end())
			break;

		int field = iter->first;
		if (field == 0 || field == 1) continue;

		auto it = override_fields.find(field);
		if (it == override_fields.end())
		{
			iso_map.erase(iter++);
			if (iter == iso_map.end())
				break;
		}
	}


	//re arrange the tpdu
	std::string nii = tpdu.substr(3, 3);
	tpdu = tpdu.substr(0, 4) + "000" + nii;

	std::vector<byte> bytes;
	std::vector<byte> temp = byteutils::hex_to_byte(tpdu);
	std::copy(temp.begin(), temp.end(), back_inserter(bytes));

	//copy the mti
	it = iso_map.find(1);
	if (it == iso_map.end())
		throw std::exception("mti is not in the source message");

	std::string mti = it->second;

	int imti = std::stoi(mti);
	imti += 10;
	mti = std::to_string(imti);
	mti = "0" + mti;
	
	temp = byteutils::hex_to_byte(mti);
	std::copy(temp.begin(), temp.end(), back_inserter(bytes));


	for (int i = 0; i < 8; i++)
		bytes.push_back(0x00);
	
	cout << endl << endl << endl;
	
	int offset_to_bitmap = 7;
	int offset_to_data = offset_to_bitmap + 8;
	
	for (int field = 2; field < 65; field++)
	{
		it = iso_map.find(field);
		if (it == iso_map.end()) // map doesnt have the field so we continue
			continue;

		//introduce a small delay to animate
		_sleep(20);


		int byte_selector = (field - 1) / 8;
		int bit_selector = (field - 1) % 8;

	

		byte temp = 1 << 7;		//get the mask setting the left most bit
		
		byte * b = &bytes[offset_to_bitmap + byte_selector];
		*b = (*b | (temp >> bit_selector));

		//append the data
		int type = FIELD_ATTRIBUTE_ARRAY[field][ATTR_INDEX_TYPE];
		int len = FIELD_ATTRIBUTE_ARRAY[field][ATTR_INDEX_LEN_DEFAULT];

		std::string data = it->second;
		std::string pack_data = "";
		std::vector<byte> temp_bytes;

		cout << "FIELD [" << std::setw(2) << std::setfill('0') << field << "] " << data << endl;

		if (type == TYPE_BCD)
			temp_bytes = byteutils::hex_to_byte(data);
		else if (type == TYPE_L_BCD)
		{
			//get the actual length of the data and set it
			int len = data.length();
			std::string hexlen = std::to_string(len);
			std::vector<byte> t = byteutils::hex_to_byte(hexlen);
		
			std::copy(t.begin(), t.end(), back_inserter(bytes));

			//append the actual data
			temp_bytes = byteutils::hex_to_byte(data);
			
		}
		else if (type == TYPE_LL_BIN)
		{
			int len = data.length();
			len /= 2; //get the byte length

			std::vector<byte> t = byteutils::get_ll_asc_length(len);
			std::copy(t.begin(), t.end(), back_inserter(bytes));

	
			temp_bytes = byteutils::hex_to_byte(data);
		}
		else if (type == TYPE_LL_ASC)
		{
			int len = data.length();
			len /= 2; //get the byte length

			std::vector<byte> t = byteutils::get_ll_asc_length(len);
			std::copy(t.begin(), t.end(), back_inserter(bytes));


			temp_bytes = byteutils::hex_to_byte(data);
		}
		else if (type == TYPE_ASC_FS)
		{
			std::string dt = byteutils::string_to_ascii(data);
			temp_bytes = byteutils::hex_to_byte(dt);
		}



		//copy the data in to the buffer
		std::copy(temp_bytes.begin(), temp_bytes.end(), back_inserter(bytes));
	
	}

	
	//print_raw_packet(bytes);

	//std::string bitmap = byteutils::byte_to_hex(bytes, offset_to_bitmap, 8);
	//cout << "BIT MAP = " << bitmap << endl << endl;
	int tot_size = bytes.size();

	byte low = static_cast<byte>(tot_size & 0xff);	//get the low byte
	byte high = static_cast<byte>((tot_size >> 8) & 0xff); // get the high byte

	std::vector<byte> ppacket;
	ppacket.push_back(high);
	ppacket.push_back(low);

	std::copy(bytes.begin(), bytes.end(), back_inserter(ppacket));

	return ppacket;
	

}

std::shared_ptr<iso8583> iso8583::instance = nullptr;



void iso8583::print_raw_packet(const std::vector<byte>& bytes)
{
	int line_len = 128;
	int lines = (bytes.size() * 2) / line_len;
	int start = 0;
	int current_line = 0;


	std::string packet = byteutils::byte_to_hex(bytes);

	while (true)
	{
		std::string line = packet.substr(start,  line_len );
		cout << line << endl;
				
		if (current_line == lines)
		{
			int len = packet.length();
			line = packet.substr(start, packet.length() - start);
			break;
		}
		start += line_len;
		current_line++;
	}

	cout << endl << endl;
}
