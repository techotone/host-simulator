#pragma once
#include <WinSock2.h>
#include <string>
#include <thread>
#include <memory>
#include <vector>
#include "defs.h"

#pragma comment(lib, "WS2_32.lib")


#define WIN32_LEAN_AND_MEAN 
#include <winsock2.h>

class comm
{
private:
	SOCKET server_socket;
	SOCKADDR_IN sockaddr_in;
	SOCKET client_socket;
	bool is_stop = false;
	std::vector<byte> client_buffer;
	bool started = false;
	int resp_delay = 0;

protected:
	
	void stop();
	void reset_stop();
	
public:
	void send(const std::vector<byte> &bytes);
	void read();
	void listen(int port);
	bool is_started();
	void set_reposnse_delay(int secs);

	virtual void update_status(std::string s) = 0;
	virtual void client_data_buffer(std::vector<byte> &buffer) = 0;
	
};

