#include "comm.h"
#include <exception>
#include <algorithm>
#include <iostream>


void comm::send(const std::vector<byte>& bytes)
{
	if (resp_delay > 0)
	{
		Sleep(resp_delay * 1000);
	}
	::send(client_socket, reinterpret_cast<char *> (const_cast<byte *>( &bytes[0])), bytes.size(), 0);
	read();
}

void comm::read()
{
	byte buffer[1024];
	client_buffer.clear();

	int read_bytes = 0;
	int length = 0;

	if (client_socket != SOCKET_ERROR)
	{
		while ((read_bytes = recv(client_socket, reinterpret_cast<char *>(&buffer[0]), sizeof(buffer), 0)) > 0)
		{

			int index = 0;
			while (read_bytes--)
				client_buffer.push_back(buffer[index++]);

			//check if the required length is recieved 
			length |= client_buffer[0];
			length <<= 8;
			length |= client_buffer[1];

			if (length + 2 == client_buffer.size())
				break;

		}

		if (client_buffer.size()  > 0)
			client_data_buffer(client_buffer);
	}
}

void comm::listen(int port)
{
	WSADATA wsadate;

	if (WSAStartup(2, &wsadate) != 0)
		throw std::exception("wsa startup failed");

	server_socket = WSASocket(AF_INET, SOCK_STREAM, 0, nullptr, 0, 0);
	if (server_socket == NULL)
		throw std::exception("creating listening socket failed");

	update_status("server socket created");


	//bind the socket for listening
	sockaddr_in.sin_family = AF_INET;
	sockaddr_in.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	sockaddr_in.sin_port = htons(port);

	int res = 0;
	if ((res = ::bind(server_socket, (PSOCKADDR)&sockaddr_in, sizeof(sockaddr_in))) == SOCKET_ERROR)
	{
		closesocket(server_socket);
		throw std::exception("binding server socket failed");
	}

	if ((res == ::listen(server_socket, 1)) == SOCKET_ERROR)
	{
		closesocket(server_socket);
		throw std::exception("listening on server socket failed");
	}

	update_status("server initialization done..");
	update_status("server is listening");
	

	//start accepting a connection
	byte buffer[1024];

	//making the server socket non blocking
	unsigned long mode = 1;
	ioctlsocket(server_socket, FIONBIO, &mode);
	started = true;
	while (!is_stop)
	{	
		
		client_socket = ::accept(server_socket, nullptr, nullptr);
		if (client_socket == SOCKET_ERROR)
		{
			int last_error  = WSAGetLastError();
			if (last_error == WSAEWOULDBLOCK)
			{
				//std::cout << ".";
				Sleep(500);
				continue;
			}

			
			is_stop = true;
			throw std::exception("accepting a socket failed");
		}

			//read the client connection
			int read_bytes = 0; 
			int length = 0;

			mode = 0;
			ioctlsocket(client_socket, FIONBIO, &mode);
			
			client_buffer.clear();
			client_buffer.reserve(sizeof(buffer));
			bool continueOuter = false;

			while ((read_bytes = recv(client_socket, reinterpret_cast<char *>(&buffer[0]), sizeof(buffer), 0)) > 0)
			{

				int index = 0;
				while (read_bytes--)
					client_buffer.push_back(buffer[index++]);

				//check if the required length is recieved 
				length |= client_buffer[0];
				length <<= 8;
				length |= client_buffer[1];

				if (length + 2 == client_buffer.size())
					break;
				else
				{
					continueOuter = true;
					break;
				}
					
	
			}
			
			if (continueOuter)
				continue;

			client_data_buffer(client_buffer);
	}

	closesocket(server_socket);
	WSACleanup();
	started = false;
	update_status("listening stopped");
}

void comm::stop()
{
	is_stop = true;
}

void comm::reset_stop()
{
	is_stop = false;
}

bool comm::is_started()
{
	return started;
}

void comm::set_reposnse_delay(int secs)
{
	resp_delay = secs;
}


