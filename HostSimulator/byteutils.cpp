#include "byteutils.h"

std::string byteutils::byte_to_hex(const std::vector<byte> bytes)
{
	
	std::stringstream str;
	str << std::hex;

	for (int i = 0; i < bytes.size(); i++)
		str << std::setw(2) << std::setfill('0') << (int)bytes[i];

	return str.str();
}




std::string byteutils::byte_to_hex(const std::vector<byte> bytes, int offset,int len)
{

	std::stringstream str;
	
	str << std::hex;

	for (int i = offset; i < offset + len; i++)
		str << std::setw(2) << std::setfill('0') << (int)bytes[i];

	return str.str();

}

std::string byteutils::ascii_to_string(const std::string & ascii_string)
{
	int num = 0;
	int len = ascii_string.length();


	std::string s;

	for (int i = 0; i < len; i += 2)
	{
		std::string ss = ascii_string.substr(i, 2);
		int num = std::stoi(ss,0,16);

		//check the number within the range
		if (num >= 32 && num <= 122)
		{
			char ch = static_cast<char> (num);
			s += (ch);
		}
		
	}

	return s;
}

std::string byteutils::string_to_ascii(const std::string & str)
{
	std::stringstream stream;

	for (int i = 0; i < str.length(); i++)
	{
		//get the ascii value of the character
		int val = static_cast<int>(str.at(i));
		stream << std::hex;
		stream << std::setw(2) << std::setfill('0') << val;
	}
	return stream.str();
}

::std::vector<byte> byteutils::hex_to_byte(const std::string & hex)
{
	std::vector<byte> bytes;
	for (int i = 0; i < hex.length(); i += 2)
	{
		byte b = static_cast<byte>(std::stoi(hex.substr(i, 2),0,16));
		bytes.push_back(b);
	}

	return bytes;
}

::std::string byteutils::int_to_hex_string(int value)
{
	std::stringstream str;
	str << std::hex;
	str << std::setw(8) << std::setfill('0') << value;

	return str.str();
}

::std::string byteutils::pad_string(std::string s, bool front, char pad_char, int len)
{
	int to_be_padded = len - s.length();
	std::string  padding;
	
	for (int i = 0; i < to_be_padded; i++)
		padding += pad_char;

	if (front)
		return padding + s;

	return s + padding;
}

::std::vector<byte> byteutils::get_ll_asc_length(int len)
{
	std::string strlen = std::to_string(len);
	strlen = pad_string(strlen, true, '0', 4);

	std::vector<byte> t = hex_to_byte(strlen);

	return t;
}

