#pragma once

#include "comm.h"
#include <thread>
#include <memory>
#include <map>

class commimpl : public comm
{
private:
	std::unique_ptr<std::thread> listen_thread;
	static commimpl instance;
	std::shared_ptr<std::map<int, std::map<int, std::string>>> cust_field_map;

	
public:
	void start_listening(int port);
	void stop_listening();
	void set_custom_field_map(std::shared_ptr<std::map<int, std::map<int, std::string>>> mp);

	// Inherited via comm
	virtual void update_status(std::string s) override;
	virtual void client_data_buffer(std::vector<byte> &buffer) override;
};
