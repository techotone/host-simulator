#pragma once
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include "defs.h"

class byteutils
{
public:
	static std::string byte_to_hex(const std::vector<byte>);
	static std::string byte_to_hex(const std::vector<byte> bytes, int offset, int len);
	static std::string ascii_to_string(const std::string &ascii_string);
	static std::string string_to_ascii(const std::string &str);
	static ::std::vector<byte> hex_to_byte(const std::string &hex);
	static ::std::string int_to_hex_string(int value);
	static ::std::string pad_string(std::string s, bool front, char pad_char,int len);
	static ::std::vector<byte> get_ll_asc_length(int len);

};